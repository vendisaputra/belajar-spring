package com.belajar.Belajar.java.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_dosen")
@Data
public class Dosen implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "nip")
    private String nip;

    @Column(name = "dsn_nama")
    private String nama;

    @Column(name = "dsn_gender")
    private String gender;

    @Column(name = "dsn_email")
    private String email;

    @Column(name = "dsn_telp")
    private String dsn_telp;

    @Column(name = "dsn_address")
    private String address;

    @Column(name = "dsn_status")
    private String status;
}
